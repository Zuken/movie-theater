<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var \yii\web\View */
/* @var $layout app\models\HallLayout */
/* @var $model \app\models\TicketForm */
/* @var $tickets app\models\Ticket[] */

$reservedSeats = "";

foreach ($tickets as $ticket) {

    $reservedSeats .= $ticket->seats;
}
$reservedSeats = explode(';', $reservedSeats);

$offSeats = explode(';', $layout->offSeats);
?>
<div class="screen mx-auto"></div>
<div class="places mt-5" style=""> 
    <?php for ($i = 1; $i < $layout->rows + 1; $i++) :?>
        <div class="row mx-auto justify-content-center" style="margin: 10px 0;">
            <span><?= "$i ряд" ?></span>

            <?php
            for ($j = 1; $j < $layout->seats + 1; $j++) {
                $seat = "$i,$j";

                if (($key = array_search($seat, $offSeats)) !== false) {
                    echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px; opacity: 0;']);
                    unset($offSeats[$key]);

                } else if (($key = array_search($seat, $reservedSeats)) !== false) {
                    echo '<div style="width: 40px; height: 40px; margin: 0 10px; line-height: 40px;text-align: center">' . $j . '</div>';
                    unset($reservedSeats[$key]);

                } else {
                    echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                }
            }
            ?>
        </div>
    <?php endfor; ?>
    <?php if (!Yii::$app->user->isGuest): ?>
    <div class="form">
        <?php  $form = ActiveForm::begin([
            'action' => '',
            'id' => 'tickets-form',
            'enableAjaxValidation' => true,
            'validationUrl' => '/ticket/validate'
        ]); ?>

        <?= $form->field($model, 'show_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

        <?= $form->field($model, 'seats')->hiddenInput(['id' => 'input-seats'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
    <?php endif; ?>
</div>

<?php $this->registerJS("
    $('#tickets-form').on('beforeSubmit', function () {
        let data = $(this).serialize();

        $.ajax({
            url: '/ticket/create',
            type: 'POST',
            data: data,
            success: function(res) {
                $('.hall').html(res);
            },
        });
        return false;
    });"
); ?>
