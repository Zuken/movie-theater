<?php  

use yii\helpers\Url;

/* @var $model app\models\Movie */

?>

<a class="poster" href="<?= Url::to(['movie/view', 'id' => $model->id]) ?>">
   <img class="col" src="/<?= $model->images_path ?>poster.jpg">
    <?php if ($model->status == 0) : ?>
        <div class="date mx-auto justify-content-center">
            <?php
                $date = DateTime::createFromFormat('Y-m-d', $model->release_date)->format('d.m');
                echo $date;
            ?>
        </div>
    <?php endif; ?>
</a>
