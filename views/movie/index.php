<?php

use app\assets\MovieAsset;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $status string */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

MovieAsset::register($this);

$this->title = 'Фильмы';
?>
<div class="mb-5 container">
    <?php Pjax::begin(); ?>
    <div class="row mx-auto justify-content-center">
        <?= Html::a('В прокате', Url::to(['movie/index', 'status' => 'now']), ['class' => 'btn btn-outline-light']) ?>
        <?= Html::a('Скоро', Url::to(['movie/index', 'status' => 'soon']), ['class' => 'btn btn-outline-light']) ?>
        <h1 class="col-12 mt-4"><?= $status == 'now' ? 'Сейчас в прокате' : 'Скоро в кино' ?></h1>
            <?=
            ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_movie',
                'summary' => '',
                'itemOptions' => [
                    'class' => 'mt-4 col-xs-4 col-sm-4 col-md-3 item',
                ]
            ]);
            ?>
    </div>
    <?php Pjax::end(); ?>
</div>
