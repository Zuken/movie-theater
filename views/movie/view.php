<?php

use app\assets\MovieViewAsset;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $movie app\models\Movie */

$this->title = $movie->name;

\yii\web\YiiAsset::register($this);
MovieViewAsset::register($this);

$shows = [];

foreach ($movie->shows as $show) {
    if ($show->status == 0) {
        for ($i = 0; $i < 5; $i++) {
            $date = date('Y-m-d', strtotime("+$i day"));
            if ($show->date == $date) {
                $shows[$date][] = $show;
            }
        }
    }
}
?>

<div class="container pt-1">
    <div class="row mb-4 mx-auto justify-content-center">
        <div class="col-md-6 col-sm-12 trailer">
            <h5 class="name" style="font-weight: 600; text-indent: 0.5em; text-align: justify;"><?= $movie->name ?></h5>

            <iframe class="mt-2 embed-responsive-item" src="/<?= $movie->images_path ?>trailer.mp4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <div class="col-md-4 col-sm-12 mt-5 label">
            <div class="information mb-3">
                <span class="title">Время фильма</span><hr>
                <span class="text"><?= $movie->getStringTime() ?></span>
            </div>
            <div class="information mb-3">
                <span class="title">Страна</span><hr>
                <span class="text"><?= $movie->country ?></span>
            </div>
            <div class="information mb-3">
                <span class="title">Жанр</span><hr>
                <span class="text"><?= $movie->genre ?></span>
            </div>
            <div class="information mb-3">
                <span class="title">Режиссер</span><hr>
                <span class="text"><?= $movie->director ?></span>
            </div>
            <?php if (!empty($movie->actors)) : ?>
            <div class="information mb-3">
                <span class="title">Актеры</span><hr>
                <span class="text">
                    <?php
                        $actors = explode(',', $movie->actors);

                        $result = $actors[0];
                        foreach ($actors as $key => $actor) {
                            if ($key == 0) {
                                continue;
                            }
                            $result .= ',<br>' . $actor;
                        }
                        echo $result;
                    ?>
                </span>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div class="container mb-3 tickets-container">
    <div class="shedul">
        <?php if ($shows) : ?>
            <h4>Расписание сеансов</h4>
            <?php foreach ($shows as $key => $day) : ?>
                <?php
                    if ($key == date('Y-m-d'))  {
                        echo '<span class="date ml-2">Сегодня, ' . date("d.m") . '</span><br>';
                    } elseif ($key == date('Y-m-d', strtotime('+1 day'))) {
                        echo '<span class="date ml-2">Завтра, ' . date("d.m", strtotime('+1 day')) . '</span><br>';
                    } else {
                        $date = DateTime::createFromFormat('Y-m-d', $key)->format('l, d.m');
                        echo '<span class="date ml-2">' . $date . '</span><br>';
                    }
                ?>
                <div class="row mt-2 pl-3 pr-3 tickets">
                    <?php foreach ($day as $show) : ?>
                    <button type="button"  class="col-xs-4 col-sm-4 col-md-3 ticket" value = "<?= $show->id ?>" data-toggle="collapse" href="#collapseHall" role="button" aria-expanded="false" aria-controls="collapseHall">
                        <span class="mx-auto"><?= DateTime::createFromFormat('H:i:s', $show->time)->format('H:i') ?></span><hr>
                        <span class="mx-auto"><?= $show->price ?></span>
                    </button>
                    <?php endforeach; ?>
                </div>

            <?php endforeach; ?>

        <?php endif; ?>
    </div>
</div>
<div class="description mb-4">
    <span>
        <?= $movie->description ?>
    </span>
    
    <div class="photos mt-4">
        <div id="photos" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                    $number = 0;
                    
                    $path = $movie->images_path . 'movie_shots';
                    $images = \yii\helpers\FileHelper::findFiles($path);
                ?>
                <?php foreach ($images as $image) : ?>
                        <div class="carousel-item <?php if ($number == 0) { $number++; echo 'active'; } ?>">
                            <?= Html::img('/' . $image, ['class' => 'd-block w-100']); ?>
                        </div>
                 <?php endforeach; ?>
            </div>

            <a class="left carousel-control" href="#photos" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#photos" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right mr-5"></span>
            </a>
        </div>
    </div>
</div>

<div class="container-fluid collapse container-hall" id="collapseHall">
    <button type="button" class="btn btn-dark" aria-label="Close" data-toggle="collapse" href="#collapseHall" role="button" aria-expanded="false" aria-controls="collapseHall">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="container hall overflow-auto"></div>
</div>
