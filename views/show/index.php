<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use app\assets\ShowAsset;

/* @var $this yii\web\View */
/* @var $moviesDataProvider yii\data\ActiveDataProvider */

$this->title = 'Расписание сеансов';

ShowAsset::register($this);
?>
<div class="shows-index mb-5">
    <h1><?= Html::encode($this->title . ' на сегодня') ?></h1>
    <?= ListView::widget([
        'dataProvider' => $moviesDataProvider,
        'itemView' => '_show',
        'summary' => '',
    ]); ?>
</div>
