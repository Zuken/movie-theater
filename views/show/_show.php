<?php

use yii\helpers\Url;

/* @var $model \app\models\Movie */

$showExists = false;

if ($model->shows) {
    foreach ($model->shows as $show) {
        if ($show->date == date('Y-m-d')) {
            $showExists  = true;
            break;
        }
    }
}
?>
<?php if ($showExists) : ?>
   <div class="container pl-5 mb-4 mx-auto justify-content-center">
      <div class="movie row">
         <a class="poster mt-4 p-0 col-sm-3 col-md-3" href="<?= Url::to(['movie/view', 'id' => $model->id]) ?>">
            <img src="/<?= $model->images_path ?>poster.jpg">
         </a>
         <div class="col-sm-5 col-md-5 mt-5">
            <div class="information">
               <span class="ml-2"><?= $model->getStringTime() ?></span>
               <span class="ml-2"><?= $model->country ?></span>
               <span class="ml-2"><?= $model->genre ?></span><br>
               <span class="ml-2 name"><a href="<?= Url::to(['movie/view', 'id' => $model->id]) ?>"><?= $model->name ?></a></span>
            </div>

            <div class="col-sm-12 col-md-5 mt-4 p-0 pl-2 trailer">
               <iframe class="embed-responsive-item" src="/<?= $model->images_path ?>trailer.mp4" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
         </div>
         <div class="col-sm-12 col-md-4 mt-4 tickets">
            <div class="row mt-2 pl-3 pr-3">
               <?php foreach ($model->shows as $show) : ?>
                  <?php if ($show->date == date('Y-m-d') && $show->status == 0) : ?>
                        <button type="button" class="col-sm-4 col-md-4 ticket" value = "<?= $show->id ?>" data-toggle="collapse" href="#collapseHall" role="button" aria-expanded="false" aria-controls="collapseHall">
                           <span class="mx-auto"><?= DateTime::createFromFormat('H:i:s', $show->time)->format('H:i') ?></span><hr>
                           <span class="mx-auto"><?= $show->price ?></span>
                        </button>
                  <?php endif; ?>
               <?php endforeach; ?>
            </div>
         </div>
      </div>
   </div>

   <div class="container-fluid collapse container-hall" id="collapseHall">
      <button type="button" class="btn btn-dark" aria-label="Close" data-toggle="collapse" href="#collapseHall" role="button" aria-expanded="false" aria-controls="collapseHall">
         <span aria-hidden="true">&times;</span>
      </button>
      <div class="container hall overflow-auto"></div>
   </div>
<?php endif; ?>
