<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserFrom */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Регистрация';
?>
<div class="users-create">

    <h1 class="mt-5"><?= Html::encode($this->title) ?></h1>

    <div class="users-form container mb-5" style="width: 400px;">

        <p class="ml-3">Пожалуйста, заполните поля для регистрации:</p>

        <?php $form = ActiveForm::begin([
            'id' => 'sign-up-form',
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg\">{input}</div>\n<div class=\"col-lg\">{error}</div>",
                'labelOptions' => ['class' => 'col-lg control-label', 'style' => 'text-align: left;'],
            ],
        ]); ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'repeatPassword')->passwordInput(['maxlength' => true]) ?>

        <div class="form-group">
            <div class="ml-3 col-lg">
                <?= Html::submitButton('Создать', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Авторизация', \yii\helpers\Url::to(['site/login']), ['class' => 'btn btn-info']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
