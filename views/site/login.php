<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

\app\assets\FormAsset::register($this);

$this->title = 'Авторизация';
?>
<div class="site-login container mt-5 mb-5" style="width: 400px;">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="ml-3">Пожалуйста, заполните поля для входа:</p>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg\">{input}</div>\n<div class=\"col-lg\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg control-label', 'style' => 'text-align: left;'],
        ],
    ]); ?>

        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg\">{input} {label}</div>\n<div class=\"col-lg\">{error}</div>",
        ]) ?>

        <div class="form-group">
            <div class="ml-3 col-lg">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                <?= Html::a('Регистрация', \yii\helpers\Url::to(['site/sign-up']), ['class' => 'btn btn-info']) ?>
            </div>
        </div>

    <?php ActiveForm::end(); ?>
</div>
