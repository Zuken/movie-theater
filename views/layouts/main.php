<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
</head>
<body>
<?php $this->beginBody() ?>

<nav class="navbar navbar-expand-lg navbar-dark bg-dark mx-auto justify-content-center">
    <div>
        <?= Html::a(Html::img('https://mdbootstrap.com/img/logo/mdb-transparent.png'), \yii\helpers\Url::to(['/']), ['class' => 'nav-link p-0']) ?>
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon" style="margin-left: 20px;"></span>
    </button>
    <div class="col-11">
        <div class="collapse navbar-collapse pb-1" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto justify-content-center">
                <li class="nav-item ml-2 mr-2">
                    <?= Html::a('Фильмы', \yii\helpers\Url::to(['/movies', 'status' => 'now']), ['class' => 'nav-link']) ?>
                </li>
                <li class="nav-item ml-2 mr-2">
                    <?= Html::a('Расписание', \yii\helpers\Url::to(['show/index']), ['class' => 'nav-link']) ?>
                </li>
            </ul>
            <?= Yii::$app->user->isGuest ? (
                    Html::tag('div', Html::a('<i class="fas fa-user mr-1"></i>Войти', \yii\helpers\Url::to(['site/login']), ['class' => 'nav-link']), ['class' => 'login'])
                ) : (
                    Html::tag('div', 
                        Html::beginForm(['site/logout'], 'post')
                            . Html::submitButton(
                                '<i class="fas fa-user mr-1"></i>Выйти',
                                //  (' . Yii::$app->user->identity->email . ')',
                                ['class' => 'btn btn-link']
                            )
                        . Html::endForm()
                    , ['class' => 'logout'])
                )?>
        </div>
    </div>
</nav>

<div class="wrap container-fluid">
    <div class="container mt-2">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
    </div>

    <?= $content ?>
</div>

<footer>
    <section class="footers bg-light pt-2 pb-3">
        <div class="container pt-4">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 footers-one mt-3 mb-3">
                    <div class="footers-logo">
                        <img src="https://mdbootstrap.com/img/logo/mdb-transparent.png" style="width:120px;">
                    </div>
                    <div class="footers-info mt-3">
                        <p>Какое-то интересное описание чего либо, и будет по красоте.</p>
                    </div>
                    <div class="social-icons"> 
                        <a class="mr-3" href="https://vk.com/"><i id="social-fb" class="fab fa-vk fa-2x social"></i></a>
                        <a class="mr-3" href="https://twitter.com"><i id="social-fb" class="fab fa-twitter fa-2x social"></i></a>
                        <a class="mr-3" href="https://facebook.com/"><i id="social-fb" class="fab fa-facebook fa-2x social"></i></a>                    
                        <a class="mr-3" href="https://instagram.com"><i id="social-fb" class="fab fa-instagram fa-2x social"></i></a>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-two">
                    <h6>Разделы:</h6>
                    <ul class="list-unstyled ml-2" style="font-size: 0.9em">
                        <li><?= Html::a('Фильмы', \yii\helpers\Url::to(['movie/index'])) ?></li>
                        <li><?= Html::a('Расписание', \yii\helpers\Url::to(['movie/index'])) ?></li>
                        <li><?= Html::a('В прокате', \yii\helpers\Url::to(['/movies', 'status' => 'now'])) ?></li>
                        <li><?= Html::a('Скоро', \yii\helpers\Url::to(['/movies', 'status' => 'soon'])) ?></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-two">
                    <h6>Социальные сети:</h6>
                    <ul class="list-unstyled ml-2" style="font-size: 0.9em">
                        <li><a href="https://twitter.com/">Twitter</a></li>
                        <li><a href="https://facebook.com/">Facebook</a></li>
                        <li><a href="https://instagram.com/">Instagram</a></li>
                        <li><a href="https://vk.com/">Вконтакте</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2 footers-two">
                    <h6>Поддержка:</h6>
                    <ul class="list-unstyled ml-2" style="font-size: 0.9em">
                        <li><a href="tel:8800#######">8 (800) ###-##-##</a></li>
                        <li><a href="mailto:support@alse.com">support@alse.com</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
