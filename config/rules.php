<?php
return [
    'admin' => 'admin/site/index',
    'admin/<action:(login|logout)>' => 'admin/site/<action>',

    'admin/workers' => 'admin/worker/index',
    'admin/worker/create' => 'admin/worker/create',
    'admin/worker/<id:\d+>' => 'admin/worker/view',
    'admin/worker/<id:\d+>/update' => 'admin/worker/update',

    'admin/users' => 'admin/user/index',
    'admin/user/create' => 'admin/user/create',
    'admin/user/<id:\d+>' => 'admin/user/view',
    'admin/user/<id:\d+>/update' => 'admin/user/update',

    'admin/halls' => 'admin/hall/index',
    'admin/hall/create' => 'admin/hall/create',
    'admin/hall/<id:\d+>' => 'admin/hall/view',
    'admin/hall/<id:\d+>/update' => 'admin/hall/update',
    'admin/hall/schema' => 'admin/hall/schema',

    'admin/movies' => 'admin/movie/index',
    'admin/movie/create' => 'admin/movie/create',
    'admin/movie/<id:\d+>' => 'admin/movie/view',
    'admin/movie/<id:\d+>/update' => 'admin/movie/update',
    'admin/POST movie/delete' => 'admin/movie/delete',

    'admin/shows' => 'admin/show/index',
    'admin/show/create' => 'admin/show/create',
    'admin/show/<id:\d+>' => 'admin/show/view',
    'admin/show/<id:\d+>/update' => 'admin/show/update',

    'admin/tickets' => 'admin/ticket/index',
    'admin/ticket/create' => 'admin/ticket/create',
    'admin/ticket/<id:\d+>' => 'admin/ticket/view',
    'admin/ticket/<id:\d+>/update' => 'admin/ticket/update',
    'admin/ticket/schema' => 'admin/ticket/schema',


    '/' => 'show/index',
    'login' => 'site/login',
    'logout' => 'site/logout',
    'sign-up' => 'site/sign-up',

    'movies' => 'movie/index',
    'movie/<id:\d+>' => 'movie/view',

    'shows' => 'show/index',
    'show/<id:\d+>' => 'show/view',

    'POST hall/schema' => 'hall/schema',

    'POST ticket/create' => 'ticket/create',
    'POST ticket/validate' => 'ticket/validate'
];
