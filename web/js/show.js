"use strict";

$(document).ready(function () {
   var ownVideos = $("iframe");
   
   $.each(ownVideos, function (i, video) {
      var frameContent = $(video).contents().find('body').html();
      
      if (frameContent) {
         $(video).contents().find('body').html(frameContent.replace("autoplay", ""));
      }
   });

    $(document).on('click', '.ticket', function () {
        let token = $('meta[name="csrf-token"]').attr("content");
        let selected = $(this).val();

        if (selected) {
            $.ajax({
                url: '/hall/schema',
                type: 'POST',
                data: {
                    'id' : selected,
                    '_csrf' : token
                },
                success: function(data) {
                    $('.hall').html(data);
                },
            });
        }
    });

    $(document).on('click', '.btn-seat', function () {
        let seat = $(this).val();
        let seats = $('#input-seats').val();

        if (this.classList.contains("off")) {
            this.classList.remove("off");
            let newValue = seats.replace(seat + ";", "");
            $('#input-seats').val(newValue);

        } else {
            this.classList.add("off");
            let  newValue = seats + seat + ";";
            $('#input-seats').val(newValue);
        }
    });
});
