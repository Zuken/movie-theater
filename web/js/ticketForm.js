
"use strict";

$(document).ready(function(){

    let showID = $('#ticket-show_id').val();
    let ticketID = $('#ticket-id').val();

    if (showID) {
        $.ajax({
            url: '/admin/ticket/schema',
            type: 'POST',
            data: {
                showID : showID,
                ticketID : ticketID
            },
            success: function(data) {
                $('#schema').html(data);
            },
        });
    }

    $(document).on('click', '.btn-seat', function () {
        let seat = $(this).val();
        let newReservedSeats = $('#ticket-seats').val();

        if (this.classList.contains("my-reserved")) {
            this.classList.remove("my-reserved");
            this.classList.remove("reserved");
            let newValue = newReservedSeats.replace(seat + ";", "");
            $('#ticket-seats').val(newValue);

        } else if (!this.classList.contains('reserved')) {
            this.classList.add("reserved");
            this.classList.add("my-reserved");
            let  newValue = newReservedSeats + seat + ";";
            $('#ticket-seats').val(newValue);
        }
    });
});
