<?php

namespace app\assets;

use yii\web\AssetBundle;

class ShowAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/show.css',
        'css/container-hall.css'
    ];
    public $js = [
        'js/show.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
