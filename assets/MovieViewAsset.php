<?php

namespace app\assets;

use yii\web\AssetBundle;

class MovieViewAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/movieView.css',
        'css/container-hall.css'
    ];
    public $js = [
        'js/show.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
