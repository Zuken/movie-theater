<?php

namespace app\models;


use yii\base\Model;

class TicketForm extends Model
{
    /** @var integer */
    public $show_id;
    /** @var integer */
    public $user_id;
    /** @var string */
    public $seats;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['show_id', 'user_id', 'seats'], 'required'],
            [['show_id', 'user_id'], 'integer'],
            [['seats'], 'string', 'max' => 100],
            ['seats', 'validateSeats']
        ];
    }

    public function validateSeats($attribute)
    {
        if (!$this->hasErrors()) {
            $seats = explode(';', $this->seats);
            $query = Ticket::find()->where(['show_id' => $this->show_id])->andWhere(['<>', 'status', 2]);
            $result = "";
            foreach ($seats as $index => $seat) {
                if (empty($seat)) {
                    continue;
                }
                $seat .= ';';
                if ($index != 0) {
                    $result .= ' OR ';
                }
                $result .= "(seats LIKE ('%;$seat%') OR seats LIKE ('$seat%'))";
            }
            $tickets = $query->andWhere("($result)")->one();
            if (!is_null($tickets)) {
                $this->seats = '';
                $this->addError($attribute, 'Выбранные места уже забронированны');
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'show_id' => 'Сеанс',
            'user_id' => 'Пользователь',
            'seats' => 'Место(а)',
        ];
    }

    public function save()
    {
        if (!$this->validate()) {

            return false;
        }
        $ticket = new Ticket();
        $ticket->user_id = $this->user_id;
        $ticket->show_id = $this->show_id;
        $ticket->seats = $this->seats;
        $ticket->status = 0;

        return $ticket->save() ? true : false;
    }
}
