<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserFrom extends Model
{
    /** @var integer */
    public $id = null;
    /** @var string */
    public $email;
    /** @var string */
    public $password;
    /** @var string */
    public $repeatPassword;
    /** @var string */
    public $status ;

    private $_user = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['email', 'password', 'repeatPassword'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
            [['password', 'repeatPassword'], 'string', 'min' => 6, 'max' => 20],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password' , 'skipOnEmpty' => false, 'message' => 'Пароли не совпадают'],
            ['status', 'integer'],
            ['status', 'default', 'value' => 0],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'E-mail',
            'repeatPassword' => 'Повторите пароль',
            'password' => 'Пароль',
            'status' => 'Статус',
        ];
    }

    public function validateEmail($attribute)
    {
        if (!$this->hasErrors()) {
            if ($this->getUser()) {
                $this->addError($attribute, 'Данный E-mail уже зарегистрирован');
            }
        }
    }

    /**
     * @return User|bool
     * @throws \yii\base\Exception
     */
    public function signUp()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = $this->status;

        return $user->save() ? true : false;
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->getUser()) {
            return Yii::$app->user->login($this->getUser(), 3600*24*30);
        }
        return false;
    }

    /**
     * Finds user by username
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
