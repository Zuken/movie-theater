<?php

namespace app\models;

/**
 * This is the model class for table "movies".
 *
 * @property int $id
 * @property string $name
 * @property string $genre
 * @property string $country
 * @property string $duration
 * @property string $director
 * @property string|null $actors
 * @property string $release_date
 * @property string $description
 * @property int $status
 * @property string $images_path
 *
 * @property Show[] $shows
 */
class Movie extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movies';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'genre', 'country', 'duration', 'director', 'release_date', 'description', 'status', 'images_path'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['name', 'country', 'director'], 'string', 'max' => 45],
            [['genre', 'actors', 'images_path'], 'string', 'max' => 100],
            [['duration'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'name' => 'Название',
            'genre' => 'Жанр',
            'country' => 'Страна',
            'duration' => 'Длительность',
            'director' => 'Режиссер',
            'actors' => 'В ролях',
            'release_date' => 'Дата выхода',
            'description' => 'Описание',
            'status' => 'Статус',
            'images_path' => 'Путь к изображениям',
        ];
    }

    /**
     * Gets query for Shows.
     *
     * @return \yii\db\ActiveQuery
     */
    public function getShows()
    {
        return $this->hasMany(Show::class, ['movie_id' => 'id']);
    }

    public function getStringTime()
    {
        $time = explode(':', $this->duration);

        $hours = 'час';
        $minutes = 'минута';

        $remainder = ltrim($time[0], '0') % 10;

        if ($remainder == 0 || ($remainder >= 5 && $remainder <= 9) || ($time[0] >= 11 && $time[0] <= 19)) {
            $hours = 'часов';
        } else if ($remainder >= 2 && $remainder <= 4) {
            $hours = 'часа';
        }

        $remainder = $time[1] % 10;

        if ($remainder == 0 || ($remainder >= 5 && $remainder <= 9) || ($time[1] >= 11 && $time[1] <= 19)) {
            $minutes = 'минут';
        } else if ($remainder >= 2 && $remainder <= 4) {
            $minutes = 'минуты';
        }

        if ($time[0] == 0) {
            return $time[1] . ' ' . $minutes;
        }
        return ltrim($time[0], '0') . ' ' . $hours . ' ' . $time[1] . ' ' . $minutes;
    }
}
