<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Ticket */
/* @var $form yii\widgets\ActiveForm */

\app\modules\admin\assets\TicketFormAsset::register($this);

if (!isset($isUpdate)) {
    $isUpdate = false;
}
?>

<div class="ticket-form">

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'show_id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

    <?php
        if ($isUpdate) {
            echo $form->field($model, 'status')->dropDownList([
                '0' => 'Забронирован',
                '1' => 'Куплен',
                '2' => 'Бронь отменена'
            ]);
        } else {
            echo $form->field($model, 'status')->hiddenInput()->label(false);
        }
    ?>

    <?= $form->field($model, 'seats')->hiddenInput() ?>

    <div id="schema">

    </div>
    <br>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
