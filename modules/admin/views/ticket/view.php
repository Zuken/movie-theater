<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Ticket */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Билеты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ticket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'show_id',
                'value' => function ($ticket) {
                    return Html::a($ticket->show_id, \yii\helpers\Url::to(['show/view', 'id' => $ticket->show_id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($ticket) {
                    return Html::a($ticket->user_id, \yii\helpers\Url::to(['user/view', 'id' => $ticket->user_id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'seats',
                'value' => function ($movie) {
                    $seats = explode(';', $movie->seats);
                    $result = '';
                    foreach ($seats as $seat) {
                        if (!empty($seat)) {
                            $info = explode(',', $seat);
                            $result .= $info[0] . ' ряд, ' . $info[1] . ' место<br>';
                        }
                    }
                    return $result;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function ($movie) {
                    if ($movie->status == 0) {
                        return 'Забронирован';
                    } elseif ($movie->status == 1) {
                        return 'Куплен';
                    }
                    return 'Бронь отменена';
                },
            ],
        ],
    ]) ?>

</div>
