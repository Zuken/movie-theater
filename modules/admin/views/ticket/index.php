<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Билеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['enablePushState' => false]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '150']
            ],
            [
                'attribute' => 'show_id',
                'value' => function ($ticket) {
                    return Html::a($ticket->show_id, \yii\helpers\Url::to(['show/view', 'id' => $ticket->show_id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($ticket) {
                    return Html::a($ticket->user_id, \yii\helpers\Url::to(['user/view', 'id' => $ticket->user_id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'seats',
                'value' => function ($movie) {
                    $seats = explode(';', $movie->seats);
                    $result = '';
                    foreach ($seats as $seat) {
                        if (!empty($seat)) {
                            $info = explode(',', $seat);
                            $result .= $info[0] . ' ряд, ' . $info[1] . ' место<br>';
                        }
                    }
                    return $result;
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'status',
                'value' => function ($show) {
                    if ($show->status == 0) {
                        return 'Забронирован';
                    } elseif ($show->status == 1) {
                        return 'Куплен';
                    }
                    return 'Бронь отменена';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    [
                        '0' => 'Забронирован',
                        '1' => 'Куплен',
                        '2' => 'Бронь отменена'
                    ],
                    [
                        'class' => 'form-control',
                        'prompt' => 'Выбрать'
                    ]
                ),
                'headerOptions' => ['width' => '140'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{view} &nbsp {update}',
                'headerOptions' => ['width' => '80'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>

</div>
