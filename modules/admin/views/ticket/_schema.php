<?php

use yii\helpers\Html;

/* @var $layout app\modules\admin\models\HallLayout */
/* @var $tickets app\modules\admin\models\Ticket[] */
/* @var $myTicket app\modules\admin\models\Ticket|null */

$reservedSeats = "";
$myReservedSeats = [];

foreach ($tickets as $ticket) {

    $reservedSeats .= $ticket->seats;
}
$reservedSeats = explode(';', $reservedSeats);
if ($myTicket) {
    $myReservedSeats = explode(';', $myTicket->seats);
}

$offSeats = explode(';', $layout->offSeats);

for ($i = 1; $i < $layout->rows + 1; $i++) :
    ?>
    <div class="row" style="margin: 10px 0;">
        <div class="row-label col-lg-1" style="line-height: 40px;">
            <span ><?= "$i ряд" ?></span>
        </div>
        <div class="col-lg-11">
            <?php
            for ($j = 1; $j < $layout->seats + 1; $j++) {
                $seat = "$i,$j";

                if (($key = array_search($seat, $offSeats)) !== false) {
                    echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px; opacity: 0;']);
                    unset($offSeats[$key]);

                } else if (($key = array_search($seat, $myReservedSeats)) !== false) {
                    echo Html::button($j, ['class' => 'btn-seat reserved my-reserved', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                    unset($myReservedSeats[$key]);

                } else if (($key = array_search($seat, $reservedSeats)) !== false) {
                    echo Html::button($j, ['class' => 'btn-seat reserved', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                    unset($reservedSeats[$key]);

                } else {
                    echo Html::button($j, ['class' => 'btn-seat', 'value' => $seat, 'style' => 'width: 40px; height: 40px; margin: 0 10px;']);
                }
            }
            ?>
        </div>
    </div>
<?php endfor; ?>
