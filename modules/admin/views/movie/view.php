<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Movie */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Фильмы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="movie-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
        if ($model->status == 1) {
            echo Html::a('Добавить киносеанс', ['show/create'], [
                'class' => 'btn btn-success',
                'data' => [
                    'method' => 'post',
                    'params' => ['movieID' => $model->id]
                ]
            ]);
        }
        ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'genre',
            'country',
            'duration',
            'director',
            'actors',
            'release_date',
            'description:ntext',
            [
                'attribute' => 'status',
                'value' => function ($movie) {
                    if ($movie->status == 0) {
                        return 'Скоро в прокате';
                    } elseif ($movie->status == 1) {
                        return 'В прокате';
                    }
                    return 'Завершен';
                },
            ],
            [
                'label' => 'Постер',
                'value' => function ($movie) {
                    return Html::img("/{$movie->images_path}poster.jpg", ['width' => '200px']);
                },
                'format' => 'html',
            ],
            [
                'label' => 'Кадры из фильма',
                'value' => function ($movie) {
                    $path = $movie->images_path . 'movie_shots';
                    $images = \yii\helpers\FileHelper::findFiles($path);
                    $result = "";
                    foreach ($images as $image) {
                        $result .= "<img src=\"/$image\" style=\"width: 400px;\">";
                    }
                    return $result;
                },
                'format' => 'raw',
            ],
            [
                'label' => 'Трейлер',
                'value' => function ($movie) {
                    return '<video controls style="width: 400px;"><source src="/' . $movie->images_path . 'trailer.mp4" type="video/mp4"></video>';
                },
                'format' => 'raw',
            ]
        ],
    ]) ?>

    <?php
    $shows = [];

    foreach ($model->shows as $show) {
        if ($show->status == 0) {
            for ($i = 0; $i < 5; $i++) {
                $date = date('Y-m-d', strtotime("+$i day"));
                if ($show->date == $date) {
                    $shows[$date][] = $show;
                }
            }
        }
    }
    if ($shows) :
        ?>

        <h2>Киносеансы</h2>

        <?php foreach ($shows as $key => $day) : ?>
        <?php
        if ($key == date('Y-m-d'))  {
            echo '<span class="date ml-2">Сегодня, ' . date("d.m") . '</span><br>';
        } elseif ($key == date('Y-m-d', strtotime('+1 day'))) {
            echo '<span class="date ml-2">Завтра, ' . date("d.m", strtotime('+1 day')) . '</span><br>';
        } else {
            $date = DateTime::createFromFormat('Y-m-d', $key)->format('l, d.m');
            echo '<span class="date ml-2">' . $date . '</span><br>';
        }
        ?>
        <div class="row mt-2 pl-3 pr-3" style="margin: 10px 0;">
            <?php foreach ($day as $show) : ?>
                <a  class="col-xs-4 col-sm-4 col-md-3 show-btn" href = "<?= \yii\helpers\Url::to(['show/view', 'id' => $show->id]) ?>">
                    <span class="mx-auto"><?= DateTime::createFromFormat('H:i:s', $show->time)->format('H:i') ?></span><hr>
                    <span class="mx-auto"><?= $show->price ?></span>
                </a>
            <?php endforeach; ?>
        </div>

    <?php endforeach; ?>
    <?php endif; ?>
</div>

<?php
$this->registerCss("
        .show-btn {
            max-width: 90px;
            width: 75px;
            margin: 5px;
            height: 56px;
            padding: 5px 0;
            text-align: center;
            border-radius: 10px;
            background: #ffffff;
            border: 1px solid #7c7c7c;
            line-height: 24px;
            text-transform: none;
        }
        .show-btn hr {
            margin-top: 0;
            margin-bottom: 0;
            width: 80%;
            margin-left: 10%;
            border-top: 1px solid #a0a0a0;
        }
        .date {
            font-weight: 600;
        }
        .show-btn, .show-btn:hover {
            color: #000000;
        }
    ");
?>
