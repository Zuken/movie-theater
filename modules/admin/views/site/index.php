<?php

use yii\widgets\DetailView;
use app\modules\admin\models\User;
use app\modules\admin\models\Worker;
use app\modules\admin\models\Movie;
use app\modules\admin\models\Hall;
use app\modules\admin\models\Show;
use app\modules\admin\models\Ticket;

/* @var $this yii\web\View */

$this->title = 'Кинотеатр';
$user = Yii::$app->getModule('admin')->get('user');
?>
<div class="site-index">

    <?php if ($user->can('listUsers')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Клиенты</h3>

            <?= DetailView::widget([
                'model' => User::class,
                'attributes' => [
                    [
                        'label' => 'Активных',
                        'value' => User::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Отключенных',
                        'value' => User::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Удаленных',
                        'value' => User::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => User::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ],
            ]) ?>

        </div>

    <?php endif; ?>

    <?php if ($user->can('listWorkers')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Сотрудники</h3>

            <?= DetailView::widget([
                'model' => Worker::class,
                'attributes' => [
                    [
                        'label' => 'Активных',
                        'value' => Worker::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Отключенных',
                        'value' => Worker::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Удаленных',
                        'value' => Worker::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => Worker::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ],
            ]) ?>

        </div>

    <?php endif; ?>

    <?php if ($user->can('listHalls')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Залы</h3>

            <?= DetailView::widget([
                'model' => Hall::class,
                'attributes' => [
                    [
                        'label' => 'Активных',
                        'value' => Hall::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Отключенных',
                        'value' => Hall::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Удаленных',
                        'value' => Hall::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => Hall::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ]
            ]) ?>

        </div>

    <?php endif; ?>

    <?php if ($user->can('listMovies')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Фильмы</h3>

            <?= DetailView::widget([
                'model' => Movie::class,
                'attributes' => [
                    [
                        'label' => 'Скоро выйдут в прокат',
                        'value' => Movie::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'В прокате',
                        'value' => Movie::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Вышли из проката',
                        'value' => Movie::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => Movie::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ]
            ]) ?>

        </div>

    <?php endif; ?>

    <?php if ($user->can('listShows')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Киносеансы</h3>

            <?= DetailView::widget([
                'model' => Show::class,
                'attributes' => [
                    [
                        'label' => 'Скоро начнутся',
                        'value' => Show::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Уже идут',
                        'value' => Show::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Завершены',
                        'value' => Show::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => Show::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ]
            ]) ?>

        </div>

    <?php endif; ?>

    <?php if ($user->can('listTickets')): ?>

        <div class="col-lg-6 col-md-6 col-sm-6">

            <h3>Билеты</h3>

            <?= DetailView::widget([
                'model' => Ticket::class,
                'attributes' => [
                    [
                        'label' => 'Забронированных',
                        'value' => Ticket::getCount(['status' => 0]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Купленных',
                        'value' => Ticket::getCount(['status' => 1]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Отмененных',
                        'value' => Ticket::getCount(['status' => 2]),
                        'contentOptions' => ['width' => '100px']
                    ],
                    [
                        'label' => 'Всего',
                        'value' => Ticket::getCount(),
                        'contentOptions' => ['width' => '100px']
                    ],
                ],
            ]) ?>

        </div>

    <?php endif; ?>

</div>
