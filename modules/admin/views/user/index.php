<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'id',
                'headerOptions' => ['width' => '150']
            ],
            'email',
            [
                'attribute' => 'status',
                'value' => function ($hall) {
                    if ($hall->status == 0) {
                        return 'Активный';
                    } elseif ($hall->status == 1) {
                        return 'Отключен';
                    }
                    return 'Удален';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    [
                        '0' => 'Активный',
                        '1' => 'Отключен',
                        '2' => 'Удален'
                    ],
                    [
                        'class' => 'form-control',
                        'prompt' => 'Выбрать'
                    ]
                ),
                'headerOptions' => ['width' => '130'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Действия',
                'template' => '{view} &nbsp {update}',
                'headerOptions' => ['width' => '80'],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
