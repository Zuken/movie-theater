<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Show */

$this->title = 'Киносеанс №' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Киносеансы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="show-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
            if ($model->status == 0) {
                echo Html::a('Забронировать места', ['ticket/create'], [
                    'class' => 'btn btn-success',
                    'data' => [
                        'method' => 'post',
                        'params' => ['showID' => $model->id]
                    ]
                ]);
            }
         ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'movieName',
                'value' => function ($show) {
                    return Html::a($show->movie->name, \yii\helpers\Url::to(['movie/view', 'id' => $show->movie->id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'hallNumber',
                'value' => function ($show) {
                    return Html::a($show->hall->number, \yii\helpers\Url::to(['hall/view', 'id' => $show->hall->id]));
                },
                'format' => 'raw'
            ],
            [
                'attribute' => 'date',
                'value' => function($show) {
                    return Yii::$app->formatter->asDate($show->date, 'dd/MM/yyyy');
                },
            ],
            [
                'attribute' => 'time',
                'value' => function($show) {
                    return Yii::$app->formatter->asTime($show->time, 'short');
                },
            ],
            'price',
            [
                'attribute' => 'status',
                'value' => function ($show) {
                    if ($show->status == 0) {
                        return 'Скоро';
                    } elseif ($show->status == 1) {
                        return 'Идёт';
                    }
                    return 'Завершен';
                },
            ],
        ],
    ]) ?>

</div>
