<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\Show */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="show-form">

    <?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true]]); ?>

    <?= $form->field($model, 'movie_id')->widget(Select2::class, [
        'name' => 'movie_id',
        'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Movie::find()->where(['status' => '1'])->all(), 'id', 'name'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите фильм', 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'hall_id')->widget(Select2::class, [
        'name' => 'hall_id',
        'data' => \yii\helpers\ArrayHelper::map(\app\modules\admin\models\Hall::find()->where(['status' => '0'])->all(), 'id', 'number'),
        'theme' => Select2::THEME_BOOTSTRAP,
        'options' => ['placeholder' => 'Выберите зал', 'autocomplete' => 'off'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::class, [
        'name' => 'date',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'readonly' => true,
        'pluginOptions' => [
            'todayHighlight' => true,
            'format' => 'yyyy-mm-dd',
            'autoclose' => true
        ]
    ]) ?>

    <?= $form->field($model, 'time')->widget(\yii\widgets\MaskedInput::class, [
        'mask' => 'h:m',
        'definitions'=>[
            'h'=>[
                'cardinality'=>2,
                'prevalidator' => [
                    ['validator'=>'^([0-2])$', 'cardinality'=>1],
                    ['validator'=>'^([0-9]|0[0-9]|1[0-9]|2[0-3])$', 'cardinality'=>2],
                ],
                'validator'=>'^([0-9]|0[0-9]|1[0-9]|2[0-3])$'
            ],
            'm'=>[
                'cardinality'=>2,
                'prevalidator' => [
                    ['validator'=>'^(0|[0-5])$', 'cardinality'=>1],
                    ['validator'=>'^([0-5]?\d)$', 'cardinality'=>2],
                ]
            ]
        ], 'options' => ['class' => 'form-control form-masked-input']]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([
        '0' => 'Скоро начнётся',
        '1' => 'Идёт',
        '2' => 'Завершен'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>
