<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Это модельный класс для таблицы "users".
 *
 * @property int $id
 * @property string $email
 * @property string $auth_key
 * @property string $password
 * @property int $status
 *
 * @property Ticket[] $tickets
 */
class User extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'auth_key', 'password', 'status'], 'required'],
            [['email'], 'string', 'max' => 40],
            [['auth_key'], 'string', 'max' => 32],
            [['password'], 'string', 'max' => 256],
            [['email'], 'unique'],
            ['status', 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'email' => 'E-mail',
            'auth_key' => 'Auth Key',
            'password' => 'Пароль',
            'status' => 'Статус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::class, ['user_id' => 'id']);
    }

    /**
     * Ищет пользователя по email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }

    /**
     * Sets a hashed password
     *
     * @param $password string
     * @throws \yii\base\Exception
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Генерирует токен
     *
     * @throws \yii\base\Exception
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }
}
