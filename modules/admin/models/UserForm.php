<?php

namespace app\modules\admin\models;

use yii\base\Model;

class UserForm extends Model
{
    /** @var integer */
    public $id;
    /** @var string */
    public $email;
    /** @var string */
    public $password;
    /** @var string */
    public $repeatPassword;
    /** @var integer */
    public $status;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    public function __construct($config = [])
    {
        parent::__construct($config);
        if($this->id != null) {
            $this->fillFields($this->id);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['email', 'status'], 'required'],
            [['email'], 'email'],
            [['email'], 'validateEmail'],
            [['password', 'repeatPassword'], 'required', 'on' => self::SCENARIO_CREATE],
            [['password', 'repeatPassword'], 'safe', 'on' => self::SCENARIO_UPDATE],
            [['password', 'repeatPassword'], 'string', 'min' => 6, 'max' => 20],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password' , 'skipOnEmpty' => false],
            [['status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'status' => 'Статус'
        ];
    }

    public function validateEmail($attribute)
    {
        if (!$this->hasErrors()) {
            if ($this->scenario == self::SCENARIO_UPDATE) {
                $user = User::findOne($this->id);
                if ($user->email != $this->email) {
                    goto EmailCheck;
                }
                return ;
            }
            EmailCheck:
            if (User::findByEmail($this->email)) {
                $this->addError($attribute, 'Данный E-mail уже зарегистрирован');
            }
        }
    }

    /**
     * Добавляет новую запись Сотрудника в БД
     *
     * @return \app\modules\admin\models\User|bool
     * @throws \yii\base\Exception
     */
    public function create()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = new User();

        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->status = $this->status;

        return $user->save() ? $user : false;
    }

    /**
     * Добавляет новую запись Сотрудника в БД
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function update()
    {
        if (!$this->validate()) {
            return false;
        }
        $user = User::findOne($this->id);

        $user->status = $this->status;

        $user->email = $this->email;

        if (!empty($this->password) && !$user->validatePassword($this->password)) {
            $user->setPassword($this->password);
        }

        return $user->save() ? true : false;
    }

    /**
     * Заполняет форму значениям из БД
     *
     * @param $id
     * @return $this
     */
    private function fillFields($id)
    {
        $user = User::findOne($id);
        $this->email = $user->email;
        $this->status = $user->status;

        return $this;
    }
}
