<?php

namespace app\modules\admin\models;

/**
 * Это модель класса для таблицы "tickets".
 *
 * @property int $id
 * @property int $show_id
 * @property int $user_id
 * @property string $seats
 * @property int $status
 *
 * @property Show $show
 * @property User $user
 */
class Ticket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['show_id', 'user_id', 'seats'], 'required'],
            [['show_id', 'user_id', 'status'], 'integer'],
            ['status', 'default', 'value' => 0],
            [['seats'], 'string', 'max' => 100],
            [['show_id'], 'exist', 'skipOnError' => true, 'targetClass' => Show::class, 'targetAttribute' => ['show_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
            ['seats', 'validateSeats'],
            ['status', 'validateStatus']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'show_id' => 'Киносеанс',
            'user_id' => 'Пользователь',
            'seats' => 'Места',
            'status' => 'Статус',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShow()
    {
        return $this->hasOne(Show::class, ['id' => 'show_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Возвращает кол-во записей
     *
     * @param array $params
     * @return int|string
     */
    public static function getCount($params = [])
    {
        return static::find()->where($params)->count();
    }

    public function validateSeats($attribute)
    {
        if (!$this->hasErrors()) {
            $seats = explode(';', $this->seats);
            $ticket = null;
            if (!is_null($this->id)) {
                $ticket = Ticket::findOne($this->id);
                $reservedSeats = explode(';', $ticket->seats);
                $seats = array_values(array_diff($seats, $reservedSeats));
                if (empty($seats)) {
                    return;
                }
            }
            $query = Ticket::find()->where(['show_id' => $this->show_id])->andWhere(['<>', 'status', 2]);
            $result = "";
            foreach ($seats as $index => $seat) {
                if (empty($seat)) {
                    continue;
                }
                $seat .= ';';
                if ($index != 0) {
                    $result .= ' OR ';
                }
                $result .= "(seats LIKE ('%;$seat%') OR seats LIKE ('$seat%'))";
            }
            $tickets = $query->andWhere("($result)")->one();
            if (!is_null($tickets)) {
                $this->seats = is_null($ticket) ? '' : $ticket->seats;
                $this->addError($attribute, 'Выбранные места уже забронированны');
            }
        }
    }

    public function validateStatus($attribute)
    {
        if (!$this->hasErrors()) {
            if (is_null($this->id)) {
                return;
            }
            $ticket = self::findOne($this->id);
            if ($this->status != $ticket->status && $ticket->status == 2) {
                $seats = explode(';', $this->seats);

                $query = Ticket::find()->where(['show_id' => $this->show_id])->andWhere(['<>', 'status', 2])->andWhere(['<>', 'id', $this->id]);
                $result = "";
                foreach ($seats as $index => $seat) {
                    if (empty($seat)) {
                        continue;
                    }
                    $seat .= ';';
                    if ($index != 0) {
                        $result .= ' OR ';
                    }
                    $result .= "(seats LIKE ('%;$seat%') OR seats LIKE ('$seat%'))";
                }
                $tickets = $query->andWhere("($result)")->one();
                if (!is_null($tickets)) {
                    $this->seats = is_null($ticket) ? '' : $ticket->seats;
                    $this->addError($attribute, 'Невозможно изменить статус, т.к. выбранные места уже забронированы');
                }
            }
        }
    }
}
