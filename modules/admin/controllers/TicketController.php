<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\User;
use app\modules\admin\models\Show;
use app\modules\admin\models\Ticket;
use app\modules\admin\models\TicketSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * TicketController реализует действия CRUD для модели Ticket.
 */
class TicketController extends Controller
{
    /**
     * Отображает страницу со списком Билетов.
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if (!$this->getUser()->can('listTickets')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $searchModel = new TicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Отображает страницу с выбранным Билетом
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        if (!$this->getUser()->can('viewTicket')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Отображает страницу с формой создания Билета.
     * Если создание произошло успешно, то перенаправит на страницу просмотра.
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        if (!$this->getUser()->can('createTicket')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new Ticket();
        $model->show_id = Yii::$app->request->post('showID');
        $model->user_id = User::findByEmail('guest')->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу с формой редактирования Билета.
     * Если обновление произошло успешно, то перенаправит на страницу просмотра.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        if (!$this->getUser()->can('updateTicket')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает схему зала выбранного киносеанса
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSchema()
    {
        if ($this->getUser()->isGuest) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $showID = Yii::$app->request->post('showID');
        $show = Show::findOne($showID);
        $layout = $show->hall->layout;

        $tickets = Ticket::find()->where(['show_id' => $showID])->andWhere(['<>', 'status', 2])->all();
        $ticketID = Yii::$app->request->post('ticketID');
        $myTicket = $ticketID ? Ticket::findOne($ticketID) : null;
        return $this->renderAjax("_schema", ['layout' => $layout, 'tickets' => $tickets, 'myTicket' => $myTicket]);
    }

    /**
     * Ищет модель Ticket по $id.
     * Если модель не найдена, то будет выдано исключение 404 HTTP.
     * @param integer $id
     * @return Ticket Загруженную модель
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Ticket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
       return Yii::$app->getModule('admin')->get('user');
    }
}
