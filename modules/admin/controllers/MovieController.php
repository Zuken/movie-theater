<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\MovieForm;
use app\modules\admin\models\Movie;
use app\modules\admin\models\MovieSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * MovieController implements the CRUD actions for Movie model.
 */
class MovieController extends Controller
{
    /**
     * Lists all Movie models.
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if (!$this->getUser()->can('listMovies')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $searchModel = new MovieSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Movie model.
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        if (!$this->getUser()->can('viewMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        if (!$this->getUser()->can('createMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new MovieForm(['scenario' => 'create']);

        if ($model->load(Yii::$app->request->post())) {
            $model->poster = UploadedFile::getInstance($model, 'poster');
            $model->movie_shots = UploadedFile::getInstances($model, 'movie_shots');
            $model->trailer = UploadedFile::getInstance($model, 'trailer');

            if ($movie = $model->create()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $movie->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Movie model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        if (!$this->getUser()->can('updateMovie')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new MovieForm(['id' => $id]);

        if ($model->load(Yii::$app->request->post())) {
            $model->poster = UploadedFile::getInstance($model, 'poster');
            $model->movie_shots = UploadedFile::getInstances($model, 'movie_shots');
            $model->trailer = UploadedFile::getInstance($model, 'trailer');

            if ($model->update()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDelete()
    {
        if ($this->getUser()->isGuest) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $filePath = Yii::$app->request->post('key');
        unlink($filePath);
        return '{}';
    }

    /**
     * Finds the Movie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Movie the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Movie::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
        return Yii::$app->getModule('admin')->get('user');
    }
}
