<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Show;
use app\modules\admin\models\ShowSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * ShowController реализует действия CRUD для модели Show.
 */
class ShowController extends Controller
{
    /**
     * Отображает страницу со списком Киносеансов.
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if (!$this->getUser()->can('listShows')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $searchModel = new ShowSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Отображает страницу с выбранным Киносеансом.
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        if (!$this->getUser()->can('viewShow')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Отображает страницу с формой создания Киносеанса.
     * Если создание произошло успешно, то перенаправит на страницу просмотра.
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        if (!$this->getUser()->can('createShow')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new Show();
        $model->movie_id = Yii::$app->request->post("movieID");

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Отображает страницу с формой редактирования Киносеанса.
     * Если обновление произошло успешно, то перенаправит на страницу просмотра.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        if (!$this->getUser()->can('updateShow')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Ищет модель Show по $id.
     * Если модель не найдена, то будет выдано исключение 404 HTTP.
     * @param integer $id
     * @return Show the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Show::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
        return Yii::$app->getModule('admin')->get('user');
    }
}
