<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\modules\admin\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if ($this->getUser()->isGuest) {
            return Yii::$app->response->redirect(['admin/site/login']);
        }

        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin()
    {
        if (!$this->getUser()->isGuest) {
            return Yii::$app->response->redirect(['admin/site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return Yii::$app->response->redirect(['admin/site/index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogout()
    {
        if ($this->getUser()->isGuest) {
            return $this->goBack();
        }

        $this->getUser()->logout();

        return Yii::$app->response->redirect(['admin/site/index']);
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
        return Yii::$app->getModule('admin')->get('user');
    }
}
