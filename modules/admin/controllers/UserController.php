<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\User;
use app\modules\admin\models\UserForm;
use app\modules\admin\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * UserController реализует действия CRUD для модели User.
 */
class UserController extends Controller
{
    /**
     * Lists all User models.
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if (!$this->getUser()->can('listUsers')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        if (!$this->getUser()->can('viewUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\Exception
     */
    public function actionCreate()
    {
        if (!$this->getUser()->can('createUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new UserForm(['scenario' => UserForm::SCENARIO_CREATE]);

        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->create()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $user->id]);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id)
    {
        if (!$this->getUser()->can('updateUser')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new UserForm(['id' => $id, 'scenario' => UserForm::SCENARIO_UPDATE]);

        if ($model->load(Yii::$app->request->post()) && $model->update()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
        return Yii::$app->getModule('admin')->get('user');
    }
}
