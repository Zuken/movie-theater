<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\HallForm;
use app\modules\admin\models\HallLayout;
use app\modules\admin\models\Hall;
use app\modules\admin\models\HallSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * HallController implements the CRUD actions for Hall model.
 */
class HallController extends Controller
{
    /**
     * Lists all Hall models.
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        if (!$this->getUser()->can('listHalls')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $searchModel = new HallSearch();
        $hallsDataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'hallsDataProvider' => $hallsDataProvider,
        ]);
    }

    /**
     * Displays a single Hall model.
     * @param integer $id
     * @return string
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionView($id)
    {
        if (!$this->getUser()->can('viewHall')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        return $this->render('view', [
            'hall' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hall model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionCreate()
    {
        if (!$this->getUser()->can('createHall')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }

        $model = new HallForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model = $model->save()) {
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Hall model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return string|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionUpdate($id)
    {
        if (!$this->getUser()->can('updateHall')) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $model = new HallForm(['id' => $id]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     * @throws ForbiddenHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionSchema()
    {
        if ($this->getUser()->isGuest) {
            throw new ForbiddenHttpException('Недостаточно прав');
        }
        $layout = HallLayout::findOne(Yii::$app->request->post('id'));

        return $this->renderAjax("_schema", ['layout' => $layout]);
    }

    /**
     * Finds the Hall model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Hall the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Hall::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }

    /**
     * @return null|object
     * @throws \yii\base\InvalidConfigException
     */
    private function getUser()
    {
        return Yii::$app->getModule('admin')->get('user');
    }
}
