<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class SchemaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/schema.css',
    ];
    public $js = [
        'js/schema.js',
    ];
}
