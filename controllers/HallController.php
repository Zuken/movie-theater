<?php

namespace app\controllers;

use Yii;
use app\models\TicketForm;
use app\models\Ticket;
use app\models\Show;
use yii\web\Controller;

/**
 * HallController реализует действия CRUD для модели Hall.
 */
class HallController extends Controller
{
    /**
     * Отображает схему зала выбранного киносеанса.
     * @return string
     */
    public function actionSchema()
    {
        $showID = Yii::$app->request->post('id');
        $show = Show::findOne($showID);
        $layout = $show->hall->layout;

        $model = new TicketForm(['show_id' => $showID, 'user_id' => Yii::$app->user->id]);
        $tickets = Ticket::find()->where(['show_id' => $showID])->andWhere(['<>', 'status', 2])->all();

        return $this->renderAjax("_schema", ['layout' => $layout, 'model' => $model, 'tickets' => $tickets]);
    }
}
