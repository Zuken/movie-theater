<?php

namespace app\controllers;

use Yii;
use app\models\TicketForm;
use app\models\Show;
use app\models\Ticket;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\web\Response;

/**
 * TicketsController реализует действия CRUD для модели Ticket.
 */
class TicketController extends Controller
{
    /**
     * Отображает форму Билета
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TicketForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return 'Места успешно забронированы';
            }

        }
        $show = Show::findOne($model->show_id);
        $layout = $show->hall->layout;

        $tickets = Ticket::find()->where(['show_id' => $model->show_id])->all();

        return $this->renderFile("@app/views/hall/_schema.php", ['layout' => $layout, 'model' => $model, 'tickets' => $tickets]);
    }

    public function actionValidate()
    {
        $model = new TicketForm();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }
}
