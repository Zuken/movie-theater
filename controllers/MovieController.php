<?php

namespace app\controllers;

use Yii;
use app\models\Movie;
use app\models\MovieSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * MovieController implements the CRUD actions for Movie model.
 */
class MovieController extends Controller
{
    /**
     * Lists all Movie models.
     * @param null $status
     * @return string
     */
    public function actionIndex($status = null)
    {
        $searchModel = new MovieSearch();
        $params = Yii::$app->request->queryParams;
        if (is_null($status)) {
            $status = 'now';
        }
        $status == 'now' ? ($params['MovieSearch']['status'] = 1) : ($params['MovieSearch']['status'] = 0);
        unset($params['status']);

        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'status' => $status
        ]);
    }

    /**
     * Displays a single Movie model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'movie' => $this->findModel($id)
        ]);
    }

    /**
     * Finds the Movie model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Movie
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Movie::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }
}
