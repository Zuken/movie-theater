<?php

namespace app\controllers;

use app\models\Movie;
use app\models\Show;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * ShowsController implements the CRUD actions for Show model.
 */
class ShowController extends Controller
{
    /**
     * Lists all Show models.
     * @return mixed
     */
    public function actionIndex()
    {
        $moviesDataProvider = new ActiveDataProvider([
            'query' => Movie::find()->where(['status' => 1])->with('shows'),
        ]);

        return $this->render('index', [
            'moviesDataProvider' => $moviesDataProvider
        ]);
    }

    /**
     * Displays a single Show model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Show model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Show the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Show::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
