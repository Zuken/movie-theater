<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m200324_100030_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'email' => $this->string(40)->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password' => $this->string(256)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue('0')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
