<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tickets`.
 */
class m200506_084459_create_tickets_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tickets', [
            'id' => $this->primaryKey(),
            'show_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'seats' => $this->string(25)->notNull(),
            'status' => $this->integer(1)->notNull()
        ]);

        $this->addForeignKey(
            'FK_Tickets_Shows',
            'tickets',
            'show_id',
            'shows',
            'id'
        );

        $this->addForeignKey(
            'FK_Tickets_Users',
            'tickets',
            'user_id',
            'users',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Tickets_Users', 'tickets');
        $this->dropForeignKey('FK_Tickets_Shows', 'tickets');
        $this->dropTable('tickets');
    }
}
