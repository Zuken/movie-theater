<?php

use yii\db\Migration;

/**
 * Handles the creation of table `halls`.
 */
class m200324_103639_create_halls_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('halls', [
            'id' => $this->primaryKey(),
            'hall_layout_id' => $this->integer()->notNull(),
            'number' => $this->tinyInteger(2)->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey(
            'FK_Halls_Hall_layouts',
            'halls',
            'hall_layout_id',
            'hall_layouts',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_Halls_Hall_layouts', 'halls');
        $this->dropTable('halls');
    }
}
